import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';

import { DomicilioService } from '../../services/domicilioService';

/**
 * Generated class for the Captura page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-captura',
  templateUrl: 'captura.html',
})
export class CapturaPage {

  isBrowser: boolean;

  codigoPostal: string;
  nombreCalle: string;
  numeroCalle: string;
  estado: string;
  ciudad: string;
  colonia: string;
  delegacion: string;
  numeroInterior: string;

  domicilios: any;
  estados: any;
  municipiosCiudades: any;
  asentamientos: any;
  ciudades: any;

  codigoEstado : any = 9;
  codigoMunicipio : any = 1;
  codigoAsentamiento : any;
  codigoCiudad : any;

  tipoAsentamiento : any = 9; //9 Colonia

  //TODO agregar codigos colonia, etc

  //URL servicios catalogos  sepomex
  baseURLFindByCP: string = 'http://54.159.19.174:8080/o/allianz-ws/catalogo.sepomex/findSepomexCodigoCP';
  baseURLFindEstados: string = 'http://54.159.19.174:8080/o/allianz-ws/catalogos.estados/findEstados';
  baseURLFindCiudadMunicipio: string = 'http://54.159.19.174:8080/o/allianz-ws/catalogos.sepomexestado/findSepomexEstadoMunicipioCiudad';
  baseURLFindAsentamiento: string = 'http://54.159.19.174:8080/o/allianz-ws/catalogo.sepomex/findSepomexByCodigoEstadoMunicipioTipoAsentamiento';
  baseURLFindCiudad: string = 'http://54.159.19.174:8080/o/allianz-ws/catalogos.sepomexestado/findSepomexEstadoCiudad';

  constructor(public navCtrl: NavController, public navParams: NavParams, private domicilioForm: DomicilioService, public events: Events, public http: Http, public platform: Platform) {

    //Checar si es browser o device
    if(this.platform.is('core') || this.platform.is('mobileweb')) {
      console.log("Its not an app");
      this.isBrowser = false;
    } else {
      console.log("Its an an app");
      this.isBrowser = true;
    }

    //Copiando valores de domicilioService
    this.codigoPostal = domicilioForm.postal_code;
    this.nombreCalle = domicilioForm.route;
    this.numeroCalle = domicilioForm.street_number;
    this.estado = domicilioForm.administrative_area_level_1;
    this.ciudad = domicilioForm.locality;
    this.colonia = domicilioForm.sublocality_level_1;
    this.delegacion = domicilioForm.administrative_area_level_3;
    this.numeroInterior = domicilioForm.num_int;



    window['LibMapaAllianz'] = { };
    window['LibMapaAllianz'].obtenerDireccionMapa = function () {
      console.log('obtenerDireccionMapa desde javascript');
      return this.valoresDireccion();
    }


    if( this.codigoPostal !== '' && this.codigoPostal !== null){
        console.log("En if codigo postal");
        console.log("Codigo postal "+this.domicilioForm.postal_code);

        //Obtiene datos por cp
        this.http.get(this.baseURLFindByCP+"?codigo_cp="+this.domicilioForm.postal_code).map(res => res.json()).subscribe(data => {

          this.domicilios = data;
          this.codigoEstado = data[0].codigoEstado;
          this.codigoMunicipio = data[0].codigoMunicipio;
          this.codigoCiudad = data[0].ciudad;
          this.tipoAsentamiento = data[0].codigoTipoAsentamiento;
  

          //Obtiene municipios y ciudades por cp
          this.http.get(this.baseURLFindCiudadMunicipio+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

            this.municipiosCiudades = data;
          
          });

          //Obtiene asentamientos
          this.http.get(this.baseURLFindAsentamiento+"?codigo_estado="+this.codigoEstado+"&codigo_municipio="+this.codigoMunicipio+"&codigo_tipo_asentamiento="+this.tipoAsentamiento).map(res => res.json()).subscribe(data => {

            this.asentamientos = data;

            for(var i=0 ; i<data.length ; i++){
              if( data[i].asentamiento === this.domicilioForm.sublocality_level_1 ){
                this.codigoAsentamiento = data[i].idAsentamientoCP;
                break;
              }

            }
          
          });

          //Obtiene ciudades
          this.http.get(this.baseURLFindCiudad+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

            this.ciudades = data;            
          
          });


        });

        //Obtiene estados
        this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
          console.log(data[0].descripcion);

          this.estados = data;
        });
        


      }else{
    
          //Obtiene estados
          this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
            console.log(data[0].codigoEstado);

            this.estados = data;
          });

           //Obtiene municipios y ciudades por codigo estado
          this.http.get(this.baseURLFindCiudadMunicipio+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

            this.municipiosCiudades = data;
          
          });
      }

    
    //Listener evento tabCapturaSelected
    events.subscribe('tabCapturaSelected', () => {

      console.log("Domicilio form en event subscribe "+domicilioForm.sublocality_level_1);

      //Copiando valores de domicilio service
      this.codigoPostal = domicilioForm.postal_code;
      this.nombreCalle = domicilioForm.route;
      this.numeroCalle = domicilioForm.street_number;
      this.estado = domicilioForm.administrative_area_level_1;
      this.ciudad = domicilioForm.locality;
      this.colonia = domicilioForm.sublocality_level_1;
      this.delegacion = domicilioForm.administrative_area_level_3;
      this.numeroInterior = domicilioForm.num_int;


      //CARGA DE CATALOGOS SEPOMEX

      if( this.codigoPostal !== '' && this.codigoPostal !== null){
        console.log("En if codigo postal");
        console.log("Codigo postal "+this.domicilioForm.postal_code);

        //Obtiene datos por cp
        this.http.get(this.baseURLFindByCP+"?codigo_cp="+this.domicilioForm.postal_code).map(res => res.json()).subscribe(data => {

          this.domicilios = data;
          this.codigoEstado = data[0].codigoEstado;
          this.codigoMunicipio = data[0].codigoMunicipio;
          this.codigoCiudad = data[0].ciudad;
          this.tipoAsentamiento = data[0].codigoTipoAsentamiento;
  

          //Obtiene municipios y ciudades por cp
          this.http.get(this.baseURLFindCiudadMunicipio+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

            this.municipiosCiudades = data;
          
          });

          //Obtiene asentamientos
          this.http.get(this.baseURLFindAsentamiento+"?codigo_estado="+this.codigoEstado+"&codigo_municipio="+this.codigoMunicipio+"&codigo_tipo_asentamiento="+this.tipoAsentamiento).map(res => res.json()).subscribe(data => {

            this.asentamientos = data;

            for(var i=0 ; i<data.length ; i++){
              if( data[i].asentamiento === this.domicilioForm.sublocality_level_1 ){
                this.codigoAsentamiento = data[i].idAsentamientoCP;
                break;
              }

            }
          
          });

          //Obtiene ciudades
          this.http.get(this.baseURLFindCiudad+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

            this.ciudades = data;            
          
          });


        });

        //Obtiene estados
        this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
          console.log(data[0].descripcion);

          this.estados = data;
        });
        


      }else{
    
          this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
            console.log(data[0].codigoEstado);

            this.estados = data;
          });


          //Obtiene municipios y ciudades por cp
          this.http.get(this.baseURLFindCiudadMunicipio+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

            this.municipiosCiudades = data;
          
          });
      }



    });

  }

  valoresDireccion(){
    console.log('En valoresDireccion, codigoPostal = ');
    return {
          valido: true,
          codigoPostal: this.domicilioForm.postal_code,
          nombreCalle: this.domicilioForm.route,
          numeroCalle: this.domicilioForm.street_number,
          numeroInterior: this.domicilioForm.num_int,
          estado: this.domicilioForm.administrative_area_level_1,
          ciudad: this.domicilioForm.locality,
          delegacion: this.domicilioForm.administrative_area_level_3,
          colonia: this.domicilioForm.sublocality_level_1
        };
  }



  blurCodigoPostal(){
    alert("Blur CP");
  }

  changeMunicipio(){
    console.log("En change municipio");
  }

}
