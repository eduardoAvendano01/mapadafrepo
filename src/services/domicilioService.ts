import {Injectable} from '@angular/core';
import { Events } from 'ionic-angular';

@Injectable()
export class DomicilioService {

   
  public postal_code: string = ''; //codigo postal
  public route: string = ''; //nombre calle
  public street_number: string = ''; //numero calle
  public administrative_area_level_1: string = ''; //Estado
  public locality: string = ''; //Ciudad
  public sublocality_level_1: string = ''; //Colonia
  public administrative_area_level_3: string = ''; //Delegacion
  public num_int: string = ''; //Numero interior


  public latitudMapa : number  = 19.436161;
  public longitudMapa : number = -99.137314;

  /*
  //Datos Mapa
 
  public geolocationLatitude: number;
  public geolocationLongitude: number;

  */
  constructor(public events: Events) {
     
  }

  //Publicando evento captura tab selected
  fireTabSelectedEvent(){
      this.events.publish('tabCapturaSelected', Date.now());
  }

}